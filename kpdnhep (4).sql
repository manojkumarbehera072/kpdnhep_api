-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2021 at 08:55 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kpdnhep`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_application` (IN `ApplicationName` VARCHAR(50), IN `ApplicationIcon` VARCHAR(100), IN `ApplicationUrl` VARCHAR(100), IN `ApplicationDescription` VARCHAR(500), OUT `IdInserted` INT(13))  NO SQL
BEGIN
insert IGNORE into application(ApplicationName,ApplicationIcon,ApplicationUrl,ApplicationDescription)
VALUES(ApplicationName,ApplicationIcon,ApplicationUrl,ApplicationDescription); 
set IdInserted=LAST_INSERT_ID();
select IdInserted;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_applications_to_user` (IN `UserName` VARCHAR(100), IN `AssignedApplications` VARCHAR(100), OUT `InsertedId` INT(14))  NO SQL
BEGIN
 DECLARE strLen    INT DEFAULT 0;
    DECLARE SubStrLen INT DEFAULT 0;
SELECT u.UserId from users u where u.UserName=UserName INTO @id;
INSERT INTO assign_application (UserId,AssignedApplications)
VALUES(@id,AssignedApplications)
ON DUPLICATE KEY UPDATE AssignedApplications = VALUES(AssignedApplications);
Set InsertedId=@id;
select InsertedId; 
insert ignore into users_group(UserId,GroupName) values(@id,'ALL');
select g.UserGroupId  from users_group g where g.UserId=@id and g.GroupName='ALL' into @groupid;
select @groupid;
DELETE FROM users_group_application WHERE UserGroupId=@groupid;
 do_this:
      LOOP
        SET strLen = CHAR_LENGTH(AssignedApplications);
        INSERT INTO users_group_application (UserGroupId,ApplicationId) VALUES(@groupid,SUBSTRING_INDEX(AssignedApplications, ',', 1));
        SET SubStrLen = CHAR_LENGTH(SUBSTRING_INDEX(AssignedApplications, ',', 1))+2;
        SET AssignedApplications = MID(AssignedApplications, SubStrLen, strLen); 

        IF AssignedApplications = '' THEN
          LEAVE do_this;
        END IF;
      END LOOP do_this;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `assign_application_to_group` (IN `NewUserGroupId` INT, IN `OldUserGroupId` INT, IN `ApplicationId` INT)  BEGIN
UPDATE users_group_application ug SET ug.UserGroupId=NewUserGroupId WHERE ug.UserGroupId=OldUserGroupId and ug.ApplicationId=ApplicationId;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `create_group` (IN `UserName` VARCHAR(100), IN `GroupName` VARCHAR(100))  NO SQL
BEGIN
select u.UserId from users u where u.UserName=UserName into @_userid;

INSERT IGNORE INTO users_group
    (UserId, GroupName)
VALUES
    (@_userid, GroupName);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_group` (IN `UserName` VARCHAR(50), IN `GroupName` VARCHAR(50))  NO SQL
BEGIN

 

delete us from users_group as us  where us.UserId =(select distinct u.UserId from users u where u.UserName=UserName) and us.GroupName=GroupName;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_applications` ()  NO SQL
select * from application$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_applications_by_username` (IN `UserName` VARCHAR(100))  NO SQL
BEGIN
DECLARE _next TEXT DEFAULT NULL;
DECLARE _nextlen INT DEFAULT NULL;
DECLARE _value TEXT DEFAULT NULL;

 

CREATE TEMPORARY TABLE new_tbl(ApplicationId int); 
select  u.UserId from users u where u.UserName=UserName into @_userid;
select  a.AssignedApplications from assign_application a where a.UserId=@_userid into @_list;

 

 

iterator:
LOOP
  IF CHAR_LENGTH(TRIM(@_list)) = 0 OR @_list IS NULL THEN
    LEAVE iterator;
  END IF;
  SET _next = SUBSTRING_INDEX(@_list,',',1);
  SET _nextlen = CHAR_LENGTH(_next);
  SET _value  = TRIM(_next);
  INSERT INTO new_tbl(ApplicationId) VALUES (_value);
  SET @_list = INSERT(@_list,1, + 1,'');
END LOOP;
select new_tbl.applicationId,application.ApplicationName,application.ApplicationIcon,application.ApplicationUrl,application.ApplicationDescription from new_tbl
inner join application on new_tbl.applicationId=application.ApplicationId;
drop table new_tbl;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_applications_by_usersgroup` (IN `UserName` VARCHAR(144), IN `GroupName` VARCHAR(100))  NO SQL
BEGIN
SELECT s.UserId from  users s WHERE s.UserName=UserName into @id;

SELECT a.ApplicationId,a.ApplicationIcon,a.ApplicationName,a.ApplicationUrl,a.ApplicationDescription from  application a 
inner join users_group_application uga
on a.ApplicationId=uga.ApplicationId
inner JOIN users_group ug
on ug.UserGroupId=uga.UserGroupId
inner JOIN users U
on u.UserId=ug.UserId
WHERE ug.UserId=@id and ug.GroupName=GroupName;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_assigned_applications_by_username` (IN `UserName` VARCHAR(100))  NO SQL
BEGIN
select  u.UserId from users u where u.UserName=username into @_userid;
select  a.AssignedApplications from assign_application a where a.UserId=@_userid ;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_edit_profile_settings` ()  NO SQL
SELECT  SettingsValue FROM `settings` WHERE SettingsType='UserEditProfile'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_user_groups` (IN `UserName` VARCHAR(100))  NO SQL
BEGIN
select u.UserId from users u where u.UserName=UserName into @id;
SELECT ug.UserGroupId ,ug.GroupName  FROM users_group ug where ug.UserId=@id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `set_edit_profile_settings` (IN `SettingsValue` BOOLEAN)  NO SQL
UPDATE `settings` SET `SettingsValue`=SettingsValue WHERE `SettingsType`='UserEditProfile'$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `user_login` (IN `UserName` VARCHAR(100), IN `Password` VARCHAR(100))  NO SQL
BEGIN
SELECT * FROM user_authentication ua WHERE ua.UserName=UserName and ua.Password=Password;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `ApplicationId` int(11) NOT NULL,
  `ApplicationName` varchar(50) DEFAULT NULL,
  `ApplicationIcon` varchar(100) DEFAULT NULL,
  `ApplicationUrl` varchar(100) DEFAULT NULL,
  `ApplicationDescription` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`ApplicationId`, `ApplicationName`, `ApplicationIcon`, `ApplicationUrl`, `ApplicationDescription`) VALUES
(1, 'DataPetrol', 'DataPetrol.png', 'https://datapetrol.kpdnhep.gov.my/', 'Detapetrol'),
(2, 'RakanKPDNHEP', 'raknkpdnhep.PNG', 'http://rakankpdnhep.infopengguna.com/', 'RAKNKPDNHEP'),
(3, 'eTribunal', 'eTribunal.png', 'https://ttpmdev.kpdnkk.gov.my/', 'eTribunal'),
(4, 'spacedeals', 'spacedeals.png', 'http://spacedeals.infopengguna.com/', 'Spacedeals'),
(5, 'eaduan', 'eaduan.png', 'https://eaduandev.kpdnhep.gov.my/', 'eaduan');

-- --------------------------------------------------------

--
-- Table structure for table `application_group`
--

CREATE TABLE `application_group` (
  `GroupId` int(11) NOT NULL,
  `GroupName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `assign_application`
--

CREATE TABLE `assign_application` (
  `AssignedApplicationId` int(11) NOT NULL,
  `UserId` int(11) DEFAULT NULL,
  `AssignedApplications` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `assign_application`
--

INSERT INTO `assign_application` (`AssignedApplicationId`, `UserId`, `AssignedApplications`) VALUES
(7, 1, '1,2,3'),
(26, 2, '1,2,3');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `SettingsId` int(11) NOT NULL,
  `SettingsType` varchar(200) DEFAULT NULL,
  `SettingsValue` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`SettingsId`, `SettingsType`, `SettingsValue`) VALUES
(1, 'UserEditProfile', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `UserId` int(11) NOT NULL,
  `UserName` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`UserId`, `UserName`) VALUES
(1, 'Manoj'),
(2, 'Dipti');

-- --------------------------------------------------------

--
-- Table structure for table `users_group`
--

CREATE TABLE `users_group` (
  `UserGroupId` int(20) NOT NULL,
  `UserId` int(20) NOT NULL,
  `GroupName` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_group`
--

INSERT INTO `users_group` (`UserGroupId`, `UserId`, `GroupName`) VALUES
(1, 1, 'ALL'),
(45, 1, 'Old'),
(43, 1, 'Social'),
(36, 1, 'Work'),
(2, 2, 'ALL'),
(19, 2, 'Old'),
(20, 2, 'Work');

-- --------------------------------------------------------

--
-- Table structure for table `users_group_application`
--

CREATE TABLE `users_group_application` (
  `UserGroupApplicationId` int(11) NOT NULL,
  `UserGroupId` int(11) DEFAULT NULL,
  `ApplicationId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users_group_application`
--

INSERT INTO `users_group_application` (`UserGroupApplicationId`, `UserGroupId`, `ApplicationId`) VALUES
(13, 2, 1),
(15, 2, 3),
(14, 19, 2),
(19, 43, 1),
(21, 43, 3),
(20, 45, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_authentication`
--

CREATE TABLE `user_authentication` (
  `UserName` varchar(200) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `FirstName` varchar(100) DEFAULT NULL,
  `LastName` varchar(100) DEFAULT NULL,
  `DisplayName` varchar(100) DEFAULT NULL,
  `Email` varchar(100) DEFAULT NULL,
  `MobileNumber` varchar(10) DEFAULT NULL,
  `Uuid` varchar(299) DEFAULT NULL,
  `Role` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_authentication`
--

INSERT INTO `user_authentication` (`UserName`, `Password`, `FirstName`, `LastName`, `DisplayName`, `Email`, `MobileNumber`, `Uuid`, `Role`) VALUES
('Manoj', '1234', 'Manoj', 'Behera', 'Manoj Behera', 'manoj@centroxy.com', '2583145780', 'cb0474d7-5fdc-4ed4-8e89-1a832931f162', ''),
('Dipti', '1234', 'Diptimayee Jena', 'Dipti', 'Diptimayee', 'diptimayee@gmail.com', '8249522311', 'b314fd4e-b975-4327-a4f9-e34aaeab714f', ''),
('admin', 'Centroxy@123', 'Admin', 'User', 'Default Admin User', 'admin@accounts.centroxy.com', '1258963254', '9e3c3d49-4c6e-4fc6-a399-9effc14f2156', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`ApplicationId`),
  ADD UNIQUE KEY `ApplicationName` (`ApplicationName`);

--
-- Indexes for table `application_group`
--
ALTER TABLE `application_group`
  ADD PRIMARY KEY (`GroupId`),
  ADD UNIQUE KEY `GroupName` (`GroupName`);

--
-- Indexes for table `assign_application`
--
ALTER TABLE `assign_application`
  ADD PRIMARY KEY (`AssignedApplicationId`),
  ADD UNIQUE KEY `UserId` (`UserId`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`SettingsId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserId`);

--
-- Indexes for table `users_group`
--
ALTER TABLE `users_group`
  ADD PRIMARY KEY (`UserGroupId`),
  ADD UNIQUE KEY `UserId` (`UserId`,`GroupName`);

--
-- Indexes for table `users_group_application`
--
ALTER TABLE `users_group_application`
  ADD PRIMARY KEY (`UserGroupApplicationId`),
  ADD UNIQUE KEY `UserGroupId` (`UserGroupId`,`ApplicationId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
  MODIFY `ApplicationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `application_group`
--
ALTER TABLE `application_group`
  MODIFY `GroupId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `assign_application`
--
ALTER TABLE `assign_application`
  MODIFY `AssignedApplicationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `SettingsId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UserId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_group`
--
ALTER TABLE `users_group`
  MODIFY `UserGroupId` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `users_group_application`
--
ALTER TABLE `users_group_application`
  MODIFY `UserGroupApplicationId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_group_application`
--
ALTER TABLE `users_group_application`
  ADD CONSTRAINT `FK_PersonOrder` FOREIGN KEY (`UserGroupId`) REFERENCES `users_group` (`UserGroupId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
